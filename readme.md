## The image gallery test

built with

* Django 1.11.11
* Python3.6
* SQLite

About structure

* Main project
    * Main APP
        * assets
            * css
                * here have all **CSS** files named with the class of correspondent view
            * js
                * here have all **JS** files named with the class of correspondent view
        * views
            * admin
                * here have a overwrite of django-admin view
            * here have all **HTML** files named with descritive names
        * here have all **PYTHON** MVC files
    * Main configuration files
        * here have all **PYTHON** configuration files 
    * Storage
        * here have all archives to compose this **MARKDOWN**
        
Why i chose this structure?
```
    The structure that i fell in love is the structure of Rails 
    i think that is the better way to organize all MVC structure.
```

----

Authenticate

|   username | password   |
|:----------:|:----------:|
| husband    | pass123456 |
| wife       | pass123456 |

---- 

GET Routes

|   path                          | description                             |
|:-------------------------------:|:---------------------------------------:|
| /                               | All users can see the images            |
| /new/photo                      | Public friends can add a photo          |
| /admin/login/?next=/admin/      | Login                                   |
| /admin/                         | Administrative Area to all models       |
| /admin/auth/user/               | List of all users                       |
| /admin/auth/user/add/           | Create a new user or add permissions    |
| /admin/auth/user/1/change/      | Edit permissions and information's user |
| /admin/gallery/photo/           | List of all Photos in the Gallery       |
| /admin/gallery/photo/add/       | Add a new photo                         |
| /admin/gallery/photo/id/change/ | Edit a created photo                    | 

Detailed description

```
  I chose / and /admin/login/?next=/admin/ to show the power of CSS and JS
  then i felt free to build and edit like i want to edit with a pretty intuitive face
  
  in the other routes, i let the pattern of DJANGO.
```

---

About Docker

Running docker image
```
    sudo docker build -t django . 
```

Expected output

![Expected outputs](/storage/docker_build_output.png)

Running the tests
```
    sudo docker run --rm django python3 manage.py test
```
----

```
    Docker is a container manager and is so awesome to use him. i hope you enjoy
```

----

Django have a explicit way to manage the auths

![django auths](/storage/django_permissions.png)
