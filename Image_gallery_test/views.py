from django.shortcuts import render
from gallery.models import Photo, PhotoForm
from django.core.files.storage import FileSystemStorage

def home_page(request):
    if request.method == "POST":
        if "id" in request.POST:
            haved_likes = Photo.objects.get(id=request.POST.get('id')).__dict__['likes']
            Photo.objects.filter(id=request.POST.get('id')).update(likes = haved_likes+1)
            return render(request, "index.html", {"photos": Photo.objects.filter(approved=True)})
        elif "date" in request.POST:
            return render(request, "index.html", {"photos": Photo.objects.filter(approved=True).order_by('date')[::-1]})
        elif "likes" in request.POST:
            return render(request, "index.html", {"photos": Photo.objects.filter(approved=True).order_by('likes')[::-1]})
    else:
        return render(request, "index.html", {"photos": Photo.objects.filter(approved=True)})

def new_photo_path(request):
    if request.method == "POST":
        filename = FileSystemStorage().save(request.FILES['image'].name, request.FILES['image'])
        Photo.objects.create(description=request.POST['description'], image=filename)
        return render(request, "form.html", {"messages": "Photo Added with success!", "form": PhotoForm()})
    else:
        return render(request, "form.html", {"form": PhotoForm(), "messages": None})