from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from .views import home_page, new_photo_path


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^new/photo/', new_photo_path),
    url(r'^$', home_page),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
