from django.contrib import admin
from .models import Photo

admin.site.site_header = "Welcome"
admin.site.index_title = "Administration"

class PhotoAdmin(admin.ModelAdmin):
    list_display = ('description', 'date', 'approved')
    readonly_fields = ["likes"]

admin.site.register(Photo, PhotoAdmin)
