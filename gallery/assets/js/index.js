function dynamicSearch() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("input_search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("photo_gallery");
    li = ul.getElementsByClassName("photo");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByClassName("photo_name")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}