from . import models
import unittest


class PhotoTest(unittest.TestCase):
    def not_create_photo_missing_image(self):
        new_photo = models.Photo.objects.create(description="Teste_case")
        self.assertIs(new_photo.save(), False)

    def not_create_photo_missing_description(self):
        new_photo = models.Photo.objects.create()
        self.assertIs(new_photo.save(), False)
