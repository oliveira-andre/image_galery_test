from django.db import models as m
from django import forms as f
from django.utils import timezone

class Photo(m.Model):
    description = m.CharField(max_length=50, blank=False)
    likes = m.IntegerField(default=0)
    approved = m.BooleanField(default=False)
    date = m.DateField(default = timezone.now, editable=False)
    image = m.ImageField(blank=False)

class PhotoForm(f.Form):
    description = f.CharField(widget=f.TextInput(attrs={"class":"form-control col-md-12", "style":"margin-bottom: 20px;"}))
    image = f.ImageField()