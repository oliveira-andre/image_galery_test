 FROM python:3.6
 ENV PYTHONUNBUFFERED 1

 RUN mkdir -p /storage/app
 WORKDIR /storage/app

 ADD requirements.txt /storage/app
 RUN pip install -r requirements.txt

 ADD . /storage/app/
